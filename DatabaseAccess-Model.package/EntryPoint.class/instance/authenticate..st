authentication
authenticate: aToken
	aToken isActive & aToken isValid
		ifFalse: [ ^ self errorUnauthorized ]