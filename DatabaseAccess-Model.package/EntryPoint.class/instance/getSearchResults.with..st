authentication
getSearchResults: queryString with: aToken
	"normally the client request will have the token but for simplicity I will send it to the proxy for all request"
		
		self authenticate: aToken.
	self getSearchResults: queryString